package com.example.puzikava.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.puzikava.R
import com.example.puzikava.SettingsValues
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_settings, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        switchMode.isChecked = (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)

        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        val prefEditor = sharedPref?.edit()
        prefEditor?.apply()
        val defLayoutValue = resources.getBoolean(R.bool.default_layout_value)

        if (sharedPref != null) {
            switchLayout.isChecked = sharedPref.getBoolean(SettingsValues.MODE_COMPACT.setting, defLayoutValue)
        }

        switchLayout.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            if (prefEditor != null) {
                with(receiver = prefEditor) {
                    putBoolean(SettingsValues.MODE_COMPACT.setting, switchLayout.isChecked)
                    apply()
                }
            }
        }

        switchMode.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            run {
                prefEditor?.putBoolean(SettingsValues.THEME_DARK.setting, switchMode.isChecked)
                prefEditor?.apply()
                if (switchMode.isChecked)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }
}