package com.example.puzikava.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.puzikava.R
import com.example.puzikava.SettingsValues
import kotlinx.android.synthetic.main.fragment_mode_chooser.*


class ModeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_mode_chooser, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        sharedPref?.edit()?.apply()
        val editor = sharedPref?.edit()

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            checkButtonDark()
            containerButtonDark.setBackgroundResource(R.drawable.radiobutton_checked)
            containerButtonLight.setBackgroundResource(R.drawable.radiobutton_unchecked)
        } else {
            checkButtonLight()
            containerButtonDark.setBackgroundResource(R.drawable.radiobutton_unchecked)
            containerButtonLight.setBackgroundResource(R.drawable.radiobutton_checked)
        }

        buttonLight.setOnClickListener{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            buttonDark.isChecked = false

            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.THEME_DARK.setting, false)
                    apply()
                }
            }
        }

        buttonDark.setOnClickListener{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            buttonLight.isChecked = false

            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.THEME_DARK.setting, true)
                    apply()
                }
            }
        }

        containerButtonLight.setOnClickListener {
            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.THEME_DARK.setting, false)
                    apply()
                }
            }
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            checkButtonLight()
        }

        containerButtonDark.setOnClickListener {
            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.THEME_DARK.setting, true)
                    apply()
                }
            }
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            checkButtonDark()
        }
    }

    private fun checkButtonLight() {
        buttonDark.isChecked = false
        buttonLight.isChecked = true
    }

    private fun checkButtonDark() {
        buttonDark.isChecked = true
        buttonLight.isChecked = false
    }
}
