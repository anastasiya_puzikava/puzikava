package com.example.puzikava.fragments

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.puzikava.MyApplication
import com.example.puzikava.R
import com.example.puzikava.adapters.IconListAdapter
import com.example.puzikava.data.AppDetails
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment(), IconListAdapter.OnLongClickListListener, IconListAdapter.OnClickListListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var apps = mutableListOf<AppDetails>()
    private lateinit var packageManager: PackageManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        packageManager = activity!!.packageManager
        val i = Intent(Intent.ACTION_MAIN, null).also {
            it.addCategory(Intent.CATEGORY_LAUNCHER)
        }

        val availableActivities = packageManager.queryIntentActivities(i, 0)
        availableActivities.forEach { resolveInfo ->
            apps.add(AppDetails(
                    resolveInfo.loadLabel(packageManager),
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.loadIcon(packageManager)
            ))
        }
        viewManager = LinearLayoutManager(activity?.applicationContext)
        viewAdapter = IconListAdapter(apps, this, this)

        recyclerView = items_list.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }

        fab_list.setOnClickListener {
            val random = (activity?.applicationContext as MyApplication).random
            (activity?.applicationContext as MyApplication)
                    .myDataSet
                    .add(0, (random.nextInt() and 0xFFFFFF) or 0xFF000000.toInt())
            recyclerView.adapter?.notifyItemInserted(0)
            recyclerView.scrollToPosition(0)
        }
    }

    override fun onLongListItemClick(position: Int) {
    }

    override fun onListItemClick(position: Int) {
        val i: Intent? = packageManager.getLaunchIntentForPackage(apps[position].name.toString())
        startActivity(i)
    }


}
