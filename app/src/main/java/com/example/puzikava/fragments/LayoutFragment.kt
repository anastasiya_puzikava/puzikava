package com.example.puzikava.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.puzikava.MyApplication
import com.example.puzikava.R
import com.example.puzikava.SettingsValues
import kotlinx.android.synthetic.main.fragment_layout_chooser.*

class LayoutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_layout_chooser, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        sharedPref?.edit()?.apply()
        val defValue = resources.getBoolean(R.bool.default_layout_value)
        val isCompactLayout = sharedPref?.getBoolean(SettingsValues.MODE_COMPACT.setting, defValue)
        val editor = sharedPref?.edit()

        if ((isCompactLayout != null) && (isCompactLayout)) {
            checkButtonDense()
            containerButtonDense.setBackgroundResource(R.drawable.radiobutton_checked)
            containerButtonStandard.setBackgroundResource(R.drawable.radiobutton_unchecked)
        } else {
            checkButtonStandard()
            containerButtonDense.setBackgroundResource(R.drawable.radiobutton_unchecked)
            containerButtonStandard.setBackgroundResource(R.drawable.radiobutton_checked)
        }

        buttonStandard.setOnClickListener {
            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.MODE_COMPACT.setting, false)
                    apply()
                }
            }
            onButtonStandardAreaClick()
        }

        buttonDense.setOnClickListener {
            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.MODE_COMPACT.setting, true)
                    apply()
                }
            }
            onButtonDenseAreaClick()
        }

        containerButtonDense.setOnClickListener {
            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.MODE_COMPACT.setting, true)
                    apply()
                }
            }
            onButtonDenseAreaClick()
        }

        containerButtonStandard.setOnClickListener {
            if (editor != null) {
                with(receiver = editor) {
                    putBoolean(SettingsValues.MODE_COMPACT.setting, false)
                    apply()
                }
            }
            onButtonStandardAreaClick()
        }
    }

    private fun onButtonStandardAreaClick() {
        (activity?.applicationContext as MyApplication).isDenseLayout = false
        checkButtonStandard()

        containerButtonDense.setBackgroundResource(R.drawable.radiobutton_unchecked)
        containerButtonStandard.setBackgroundResource(R.drawable.radiobutton_checked)
    }

    private fun onButtonDenseAreaClick() {
        (this.activity?.applicationContext as MyApplication).isDenseLayout = true
        checkButtonDense()

        containerButtonDense.setBackgroundResource(R.drawable.radiobutton_checked)
        containerButtonStandard.setBackgroundResource(R.drawable.radiobutton_unchecked)
    }

    private fun checkButtonDense() {
        buttonDense.isChecked = true
        buttonStandard.isChecked = false
    }

    private fun checkButtonStandard() {
        buttonDense.isChecked = false
        buttonStandard.isChecked = true
    }
}
