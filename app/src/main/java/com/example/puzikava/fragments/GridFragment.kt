package com.example.puzikava.fragments

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.puzikava.R
import com.example.puzikava.SettingsValues
import com.example.puzikava.adapters.IconGridAdapter
import com.example.puzikava.data.AppDetails
import com.example.puzikava.decorators.IconDecorator
import kotlinx.android.synthetic.main.fragment_grid_launcher.*
import kotlin.math.roundToInt

class GridFragment : Fragment(), IconGridAdapter.OnLongClickGridListener, IconGridAdapter.OnClickGridListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var apps = mutableListOf<AppDetails>()
    private lateinit var packageManager: PackageManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_grid_launcher, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val columnsCount: Int
        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        sharedPref?.edit()?.apply()
        val defValue = resources.getBoolean(R.bool.default_layout_value)
        val isCompactLayout = sharedPref?.getBoolean(SettingsValues.MODE_COMPACT.setting, defValue)

        columnsCount = if (isCompactLayout != null) {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                if (isCompactLayout)
                    7 else 6
            } else {
                if (isCompactLayout)
                    5 else 4
            }
        } else {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                4 else 6
        }

        packageManager = activity!!.packageManager
        val i = Intent(Intent.ACTION_MAIN, null).also(fun(it: Intent) {
            it.addCategory(Intent.CATEGORY_LAUNCHER)
        })

        val availableActivities = packageManager.queryIntentActivities(i, 0)
        for (resolveInfo in availableActivities) {
            apps.add(AppDetails(
                    resolveInfo.loadLabel(packageManager),
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.loadIcon(packageManager)
            ))
        }

        val p = Point()
        activity?.windowManager?.defaultDisplay?.getSize(p)
        val screenWidth = p.x

        val offset = resources.getDimensionPixelSize(R.dimen.icon_offset)
        val sumOffset = 1.5f * (3 * offset + (columnsCount - 1) * offset / 2)
        val elementWidth = ((screenWidth.minus(sumOffset)).div(columnsCount)).roundToInt()

        viewManager = GridLayoutManager(activity?.applicationContext, columnsCount)

        val delta = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            40
        else
            15

        viewAdapter = IconGridAdapter(apps, this, elementWidth - delta)

        recyclerView = icons_grid.apply {
            addItemDecoration(IconDecorator(resources.getDimensionPixelSize(R.dimen.icon_offset)))
            layoutManager = viewManager
            adapter = viewAdapter
        }

        fab_grid.setOnClickListener {
        }

    }

    override fun onLongGridItemClick(position: Int) {}

    override fun onGridItemClick(position: Int) {
        packageManager.getLaunchIntentForPackage(apps[position].name.toString()).also {
            startActivity(it)
        }
    }
}