package com.example.puzikava

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import kotlin.random.Random

class MyApplication : Application() {

    private val defaultLayoutState = false

    var isDenseLayout: Boolean = defaultLayoutState

    var myDataSet = ArrayList<Int>()

    val random = Random(2452)

    init {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        isDenseLayout = false
        for (a in 1..1000) {
            myDataSet.add((random.nextInt() and 0xFFFFFF) or 0xFF000000.toInt())
        }
    }
}