package com.example.puzikava

enum class SettingsValues(val setting: String) {
    APP_PREFERENCES("app_pref"),
    MODE_COMPACT("compact"),
    THEME_DARK("dark"),
    IS_LAUNCHER_VISITED("true");
}