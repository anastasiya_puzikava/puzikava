package com.example.puzikava.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.puzikava.R
import com.example.puzikava.SettingsValues
import com.example.puzikava.adapters.WelcomeAdapter
import com.example.puzikava.fragments.GreetingFragment
import com.example.puzikava.fragments.LayoutFragment
import com.example.puzikava.fragments.ModeFragment
import kotlinx.android.synthetic.main.activity_welcome_page.*

class WelcomePageActivity : AppCompatActivity() {

    private lateinit var pagerAdapter: PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)

        val sharedPreferences = getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        if (!sharedPreferences.getBoolean(SettingsValues.IS_LAUNCHER_VISITED.setting,true)) {
            skipStart()
            return
        }

        checkSettingsDarkMode()

        prepareDots()
        val fragments = listOf(
                GreetingFragment(),
                ModeFragment(),
                LayoutFragment()
        )

        pagerAdapter = WelcomeAdapter(supportFragmentManager, fragments)
        pager.adapter = pagerAdapter

        buttonNextFragment.setOnClickListener(fun(_: View) {
            with(pager, {
                if (currentItem < 2) {
                    return@with pager.arrowScroll(View.FOCUS_RIGHT)
                } else {
                    return@with Intent().also(fun(it: Intent) {
                        it.setClass(applicationContext, MainActivity::class.java)
                        startActivity(it)
                    })
                }
            })
        })

        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                prepareDots()
            }

        })

    }

    private fun skipStart() {
        startLauncher()
        checkSettingsDarkMode()
        finish()
    }

    private fun checkSettingsDarkMode() {
        val darkMode = getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
                .getBoolean(SettingsValues.THEME_DARK.setting, false)
        if (darkMode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    private fun startLauncher() {
        Intent().also(fun(it: Intent) {
            it.setClass(applicationContext, MainActivity::class.java)
            startActivity(it)
        })
    }

    override fun onBackPressed() {

        if (pager.currentItem != 0) {
            pager.arrowScroll(View.FOCUS_LEFT)
        } else
            super.onBackPressed()
    }

    private fun prepareDots() {
        if (pageIndicator.childCount > 0) {
            pageIndicator.removeAllViews()
        }

        val dots = mutableListOf<ImageView>()

        for (i in 0..2) {
            dots += ImageView(this)

            if (i == pager.currentItem) {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dot))
            } else {
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.inactive_dot))
            }

            val layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ).also(fun(it: LinearLayout.LayoutParams) {
                it.setMargins(12, 0, 12, 0)
            })
            pageIndicator.addView(dots[i], layoutParams)
        }
    }
}
