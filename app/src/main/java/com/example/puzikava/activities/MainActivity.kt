package com.example.puzikava.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.puzikava.R
import com.example.puzikava.SettingsValues
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.drawer_nav_header_navigation.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var mContext: Context
    private var isStartDestination = true
    private var idDestination = 0
    private var startDestinationId = 0

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        mContext = applicationContext

        getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE).also {
            it.edit().putBoolean(SettingsValues.IS_LAUNCHER_VISITED.setting, false).apply()
        }

        setContentView(R.layout.activity_main)
        findViewById<Toolbar>(R.id.main_content_toolbar).also {
            setSupportActionBar(it)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        startDestinationId = navController.graph.startDestination

        navController.addOnDestinationChangedListener {_, destination, _ ->
            isStartDestination = destination.id == startDestinationId
            idDestination = destination.id
        }

        appBarConfiguration = AppBarConfiguration(
                setOf(
                        R.id.launcherFragment, R.id.listFragment
                ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.nav_drawer, menu)
        nav_header_image.setOnClickListener{
            val intent = Intent()
            intent.run(fun Intent.() {
                setClass(it.context, ProfileActivity::class.java)
                startActivity(this)
            })
        }
        return true
    }

    override fun onBackPressed() {
        if (idDestination == 0 || idDestination == startDestinationId)
            super.onBackPressed()
        else {
            findNavController(R.id.nav_host_fragment).navigate(startDestinationId)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
