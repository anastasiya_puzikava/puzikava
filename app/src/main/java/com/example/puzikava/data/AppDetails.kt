package com.example.puzikava.data

import android.graphics.drawable.Drawable

data class AppDetails(var label: CharSequence, var name: CharSequence, var icon: Drawable)