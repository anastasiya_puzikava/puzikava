package com.example.puzikava.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.puzikava.R
import com.example.puzikava.data.AppDetails

class IconListAdapter(
        private val myDataset: MutableList<AppDetails>,
        private val mOnClickListListener: OnClickListListener,
        private val mOnLongClickListListener: OnLongClickListListener) :
        RecyclerView.Adapter<IconListAdapter.IconListItemViewHolder>() {

    class IconListItemViewHolder(
            v: View,
            private var onLongClickListListener: OnLongClickListListener,
            private var onClickListListener: OnClickListListener) :
            RecyclerView.ViewHolder(v), View.OnLongClickListener, View.OnClickListener {

        val viewCell: View = v.findViewById(R.id.just_view)
        val appLabel: TextView = v.findViewById(R.id.textView8)

        override fun onLongClick(v: View?): Boolean {
            onLongClickListListener.onLongListItemClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            onClickListListener.onListItemClick(adapterPosition)
        }

        init {
            v.setOnLongClickListener(this)
            v.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): IconListItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return IconListItemViewHolder(v, mOnLongClickListListener, mOnClickListListener)
    }

    override fun onBindViewHolder(holder: IconListItemViewHolder, position: Int) {
        holder.viewCell.background = myDataset[position].icon
        holder.appLabel.text = myDataset[position].label
    }

    override fun getItemCount() = myDataset.size

    interface OnLongClickListListener {
        fun onLongListItemClick(position: Int)
    }

    interface OnClickListListener {
        fun onListItemClick(position: Int)
    }

}