package com.example.puzikava.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.puzikava.R
import com.example.puzikava.data.AppDetails

class IconGridAdapter(
        private val myDataset: MutableList<AppDetails>,
        private val mOnClickGridListener: OnClickGridListener,
        private val itemWidth: Int) :
        RecyclerView.Adapter<IconGridAdapter.IconGridItemViewHolder>() {

    class IconGridItemViewHolder(
            v: View,
            private val onClickGridListener: OnClickGridListener) :
            RecyclerView.ViewHolder(v),
            View.OnLongClickListener,
            View.OnClickListener {

        val viewCell: View = v.findViewById(R.id.iconColored)
        val iconContainer: ConstraintLayout = v.findViewById(R.id.iconContainer)
        val appName: TextView = v.findViewById(R.id.appName)

        init {
            itemView.setOnLongClickListener(this)
            itemView.setOnClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            return true
        }

        override fun onClick(v: View?) {
            onClickGridListener.onGridItemClick(adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): IconGridItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_grid, parent, false)
        return IconGridItemViewHolder(v, mOnClickGridListener)
    }

    override fun onBindViewHolder(holder: IconGridItemViewHolder, position: Int) {
        holder.viewCell.background = myDataset[position].icon
        holder.appName.text = myDataset[position].label
        val layoutParams = ConstraintLayout.LayoutParams(itemWidth, itemWidth)
        holder.iconContainer.layoutParams = layoutParams
    }

    override fun getItemCount() = myDataset.size

    interface OnLongClickGridListener {
        fun onLongGridItemClick(position: Int)
    }

    interface OnClickGridListener {
        fun onGridItemClick(position: Int)
    }

}